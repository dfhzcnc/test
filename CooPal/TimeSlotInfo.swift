//
//  TimeSlotInfo.swift
//  CooPal
//
//  Created by Peiyang Shangguan on 6/16/16.
//  Copyright © 2016 Lidongfang Guo. All rights reserved.
//

import Foundation
import EVReflection

class TimeSlotInfo : EVObject {
    var trainerID       = ""
    var slot            = ""
    var availLocalDate  = ""
    var timeSlotID      = ""
}