//
//  ReviewInfo.swift
//  CooPal
//
//  Created by Peiyang Shangguan on 6/16/16.
//  Copyright © 2016 Lidongfang Guo. All rights reserved.
//

import Foundation
import EVReflection

class ReviewInfo : EVObject {
    var firstName   = ""
    var lastName    = ""
    var orderID     = ""
    var id          = 0
    var reviewTime  = ""
    var reviewType  = 0
    var rate        = 0
    var toUserID    = 0
    var fromUserID  = 0
    var review      = ""
}