//
//  TrainerInfo.swift
//  CooPal
//
//  Created by Peiyang Shangguan on 6/16/16.
//  Copyright © 2016 Lidongfang Guo. All rights reserved.
//

import Foundation
import EVReflection

class TrainerInfo : EVObject {
    var profileInfo = ProfileInfo()
    var imageInfoList = [ImageInfo]()
    var timeSlotInfoList = [TimeSlotInfo]()
    var timeSlotMap = [String: String]()
    var trainerPlaceInfoList = [TrainerPlaceInfo]()
    var reviewInfoList = [ReviewInfo]()
}