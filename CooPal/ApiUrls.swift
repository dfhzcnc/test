//
//  ApiUrls.swift
//  CooPal
//
//  Created by Peiyang Shangguan on 6/16/16.
//  Copyright © 2016 Lidongfang Guo. All rights reserved.
//

import Foundation

struct ApiUrls {
    // Image
    static let IMAGE_PREFIX = "http://res.cloudinary.com/coopals/image/upload/w_0.6/";
    static let AVATAR_PREFIX = "http://res.cloudinary.com/coopals/image/upload/g_face,c_thumb,w_150,h_150/c_scale,w_150,r_max/"
    static let UPLOAD_PICTURE_URL = "http://159.203.212.22:5000/upload";
    static let DELETE_PICTURE_URL = "http://159.203.212.22:5000/deleteImg";
    
    // Trainer
    static let GET_TRAINER_INFO = "http://159.203.212.22:5000/coachInfo/";
    static let DOWNLOAD_TRAINER = "http://159.203.212.22:5000/getImg/1";
    
    // Gym
    static let GET_GYM_URL = "http://159.203.212.22:5000/searchGym2";
    
    // User profile
    static let GET_USER_INFO = "http://159.203.212.22:5000/myProfile";
    static let UPDATE_USER_PROFILE_URL = "http://159.203.212.22:5000/updateProfile";
    static let GET_USER_AVATAR_REVIEW = "http://159.203.212.22:5000/customerInfo/";
    
    // Login
    static let TOKEN_LOGIN_URL = "http://159.203.212.22:5000/tokenLogin";
    static let LOGIN_URL = "http://159.203.212.22:5000/login";
    static let SIGNUP_URL = "http://159.203.212.22:5000/createAccount";
    static let FORGET_PASSWD_URL = "";
    static let FB_LOGIN_URL = "http://159.203.212.22:5000/loginFromFB";
    
    // Chat
    static let CHAT_MESSAGE_URL = "http://159.203.212.22:5000/message";
    static let POST_MESSAGE_URL = "http://159.203.212.22:5000/postMessage";
    
    // Reservation
    static let GET_RESV_FOR_USER = "http://159.203.212.22:5000/getRevs/customer/";
    static let GET_RESV_FOR_TRAINER = "http://159.203.212.22:5000/getRevs/coach/";
    static let CONFIRM_RESV = "http://159.203.212.22:5000/confirmOrder";
    static let CANCEL_RESV = "http://159.203.212.22:5000/cancelOrder";
    static let POST_REVIEW = "http://159.203.212.22:5000/postReview";
    
    // Verify Coach
    static let VERIFY_TRAINER_URL = "http://159.203.212.22:5000/verifyCoach";
    
    // Filter
    static let FILTER_COACH_URL = "http://159.203.212.22:5000/searchCoach";
    
    // Upload coach schedule info
    static let UPLOAD_SCHEDULE = "http://159.203.212.22:5000/addTrainerTimeLocation";
    
    // Search coach list
    static let SEARCH_COACH_URL = "http://159.203.212.22:5000/searchCoach";
    static let SEARCH_GYM_HAS_COACH = "http://159.203.212.22:5000/searchCoach2";
    
    // Get lat and long by zip
    static let GET_LATLNG_BY_ZIP = "http://159.203.212.22:5000/getLatLon?zip=";
    
    // Order a class
    static let ORDER_CLASS_URL = "http://159.203.212.22:5000/order";
    
    // Verify user phone
    static let VERIFY_PHONE_URL = "http://159.203.212.22:5000/verifyPhone/sms";
    static let ACTIVATE_PHONE_URL = "http://159.203.212.22:5000/verifyPhone/activate";
    
    
    // Update bank account
    static let UPDATE_BANK_ACCOUNT = "http://159.203.212.22:5000/editBankAccount";
    
    // Resend to verify email
    static let RESEND_EMAIL = "http://159.203.212.22:5000/verifyEmail";
    
    // Register phone
    static let REGISTER_PHONE = "http://159.203.212.22:5000/registerPhone";
    
    //Invite friend in contact list
    static let INVITE_FRIEND_PHONE = "http://159.203.212.22:5000/registerPhone";
}