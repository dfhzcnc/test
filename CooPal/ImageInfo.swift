//
//  ImageInfo.swift
//  CooPal
//
//  Created by Peiyang Shangguan on 6/16/16.
//  Copyright © 2016 Lidongfang Guo. All rights reserved.
//

import Foundation
import EVReflection

class ImageInfo : EVObject {
    var  photoID  = ""
    var  userID   = ""
    var  link     = ""
    var  pictype  = ""
}