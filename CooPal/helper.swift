//
//  helper.swift
//  CooPal
//
//  Created by Lidongfang Guo on 6/16/16.
//  Copyright © 2016 Lidongfang Guo. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import SwiftyJSON
import Kingfisher

class helper: UIView {
    
    //MARK: Fetch Trainer Info For ALLTrainersTableView
    static func fetchTrainer() {
        
    }
    
    //MARK: make image view round shape
    static func circularImage(photoImageView: UIImageView?)
    {
        photoImageView!.layer.frame = CGRectInset(photoImageView!.layer.frame, 0, 0)
        photoImageView!.layer.borderColor = UIColor.whiteColor().CGColor
        photoImageView!.layer.cornerRadius = photoImageView!.frame.height/2
        photoImageView!.layer.masksToBounds = false
        photoImageView!.clipsToBounds = true
        photoImageView!.layer.borderWidth = 1.5
        photoImageView!.contentMode = UIViewContentMode.ScaleAspectFill
    }

    func fetchURL() {
        
    }
    
    func fetch(){
        
    }
}