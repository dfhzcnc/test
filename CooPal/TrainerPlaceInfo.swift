//
//  TrainerPlaceInfo.swift
//  CooPal
//
//  Created by Peiyang Shangguan on 6/16/16.
//  Copyright © 2016 Lidongfang Guo. All rights reserved.
//

import Foundation
import EVReflection

class TrainerPlaceInfo : EVObject {
    var zipcode     = ""
    var latitude    = ""
    var id          = ""
    var address     = ""
    var state       = ""
    var placeType   = ""
    var city        = ""
    var gymID       = ""
    var trainerID   = ""
    var name        = ""
    var phoneNumber = ""
    var longitude   = ""
}