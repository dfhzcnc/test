//
//  AllTrainersTableView.swift
//  CooPal
//
//  Created by Lidongfang Guo on 6/16/16.
//  Copyright © 2016 Lidongfang Guo. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class AllTrainersTableView: UITableViewController {
    
    // Trainer list that will request from server and display
    var trainerList = [TrainerInfo]() {
        didSet{
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Request trainers
        TrainerInfoAccess.searchTrainer("1",
                                        token: "69f94bbcca2c4dfcaa88d3691f881425",
                                        filterInfo: FilterInfo())
        {
            self.trainerList = $0 ?? [TrainerInfo]()
        }
    }
    
    //==============================================================================================================
    //MARK:Two necessary funcs to set Prototype cell
    //#1 Set Cell Numbers
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trainerList.count
    }
    //#2 Load Data to Prototype1 Cell
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let dequeued = tableView.dequeueReusableCellWithIdentifier("prototype1", forIndexPath: indexPath) as! AllTrainersTableCell
        
        let trainer = trainerList[indexPath.item]
        
        //set trainner large image
        var counter: Int = 0
        var picbox = [UIImageView]()
        for cell in trainer.imageInfoList where cell.pictype == "D" {
            
            let w = self.view.frame.width
            let h = self.view.frame.height
            print(w)
            print(h)
            let URL = cell.link
            let tmp = UIImageView(frame: CGRectMake(CGFloat(counter)*w , 0, w, w))
            tmp.contentMode = UIViewContentMode.ScaleAspectFill
            tmp.clipsToBounds = true
            tmp.kf_setImageWithURL(NSURL(string: ApiUrls.IMAGE_PREFIX + URL)!)
            counter += 1
            picbox.append(tmp)
        }
        
        print(picbox.count)
        
        //set scrollview contect size, paging, scrollbar
        dequeued.scrollView.contentSize.width = CGFloat(counter)*(self.view.frame.width)
        dequeued.scrollView.pagingEnabled = true
        dequeued.scrollView.showsHorizontalScrollIndicator = false
        print(dequeued.scrollView.contentSize.width)
        
        for imageview in picbox {
            dequeued.scrollView.addSubview(imageview)
        }
        
        
        
        //set trainner small image
        for image in trainer.imageInfoList where image.pictype == "A" {
            dequeued.trainerImageSmall!.kf_setImageWithURL(NSURL(string: ApiUrls.AVATAR_PREFIX + image.link)!)
            helper.circularImage(dequeued.trainerImageSmall!)
        }
        
        //set name, location, price, rate
        dequeued.trainerName.text = trainer.profileInfo.firstName + " " + trainer.profileInfo.lastName
        dequeued.trainerLocation.text = trainer.profileInfo.location
        dequeued.trainerPrice.text = "  $" + trainer.profileInfo.price + " per lesson"
        dequeued.rating.settings.fillMode = .Half
        dequeued.rating.settings.starSize = 20
        dequeued.rating.settings.updateOnTouch = false
        dequeued.rating.rating = Double(trainer.profileInfo.rating) ?? 2.8
        
        //return cell to load
        return dequeued
    }
    //===============================================================================================================
}
