//
//  ProfileInfo.swift
//  CooPal
//
//  Created by Peiyang Shangguan on 6/16/16.
//  Copyright © 2016 Lidongfang Guo. All rights reserved.
//

import Foundation
import EVReflection

class ProfileInfo : EVObject {
    var introduction     = ""
    var userID           = ""
    var firstName        = ""
    var price            = ""
    var gender           = ""
    var location         = ""
    var about            = ""
    var lastName         = ""
    var title            = ""
    var rating           = ""
    var nickName         = ""
    var member_since     = ""
    var specialization   = ""
    var languageList     = ""
}