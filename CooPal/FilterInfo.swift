//
//  FilterInfo.swift
//  CooPal
//
//  Created by Peiyang Shangguan on 6/16/16.
//  Copyright © 2016 Lidongfang Guo. All rights reserved.
//

import Foundation
import EVReflection

class FilterInfo : EVObject {
    var gender          = 2
    var beginDate       = ""
    var endDate         = ""
    var LBLat           = -1.0
    var LBLon           = -1.0
    var RULat           = -1.0
    var RULon           = -1.0
    var languageList    = ""
    var priceMin        = ""
    var priceMax        = ""
    var specialization  = ""
    
    required init() {
        // Generate default filter info
        gender = 2;     // 0 for female, 1 for male, 2 for either
        
        // get the current date and time
        let currentDateTime = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let daysToAdd = 7.0
        let toDate = NSDate(timeInterval: 60*60*24*daysToAdd, sinceDate: currentDateTime)
        beginDate = formatter.stringFromDate(currentDateTime)
        endDate = formatter.stringFromDate(toDate)
        
        let latitude = 32.929636
        let longitude = -96.761624
        LBLat = latitude - 0.1;
        LBLon = longitude - 0.1;
        RULat = latitude + 0.1;
        RULon = longitude + 0.1;
        priceMin = "0";
        priceMax = "9999";
        specialization = "000000";
        languageList = "000";
        
        super.init()
    }
}
