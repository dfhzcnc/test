//
//  AllTrainersTableCell.swift
//  CooPal
//
//  Created by Lidongfang Guo on 6/16/16.
//  Copyright © 2016 Lidongfang Guo. All rights reserved.
//

import UIKit
import Cosmos

class AllTrainersTableCell: UITableViewCell {
    
    //MARK: ALL properties in prototype1 cell
    
    
    @IBOutlet weak var trainerImageSmall: UIImageView!
    @IBOutlet weak var trainerName: UILabel!
    @IBOutlet weak var trainerLocation: UILabel!
    @IBOutlet weak var trainerPrice: UILabel!
    @IBOutlet weak var rating: CosmosView!
    
    @IBOutlet weak var trainerTitle: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!

}
