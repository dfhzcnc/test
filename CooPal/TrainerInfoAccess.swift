//
//  TrainerInfoAccess.swift
//  CooPal
//
//  Created by Peiyang Shangguan on 6/16/16.
//  Copyright © 2016 Lidongfang Guo. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class TrainerInfoAccess {
    
    // Search trainer by filter info
    // Returns a list of TrainerInfo
    static func searchTrainer(userID: String,
                              token: String,
                              filterInfo: FilterInfo,
                              listener: [TrainerInfo]? -> Void) -> Void
    {
        var parameters = filterInfo.toDictionary() as! [String : AnyObject]
        parameters.updateValue(userID, forKey: "userID")
        parameters.updateValue(token, forKey: "token")
        
        Alamofire.request(.POST, ApiUrls.SEARCH_COACH_URL, parameters: parameters, encoding: .JSON)
            .validate()
            .responseData { response in
                // Parse json from response
                let json = JSON(data: response.data!)
                
                // Check if status is correct, if not, call listener with nil
                if let status = json["loginstatus"].string {
                    if status != "token login successful1" {
                        listener(nil)
                    }
                } else {
                    listener(nil)
                }
                
                // Get trainers
                var trainers = [TrainerInfo]()
                for i in 1...json.count {
                    // Get json for a trainer
                    let trainerInfoJson = json[String(i)]
                    
                    // If is status, continue to next
                    if trainerInfoJson.count != 5{
                        continue
                    }
                    
                    // Create a new trainer info
                    let trainer = TrainerInfo()
                    
                    // Get profile
                    let profileJson = trainerInfoJson["profile"].rawString()
                    let profile = ProfileInfo(json: profileJson)
                    trainer.profileInfo = profile
                    
                    // Get time slots
                    let timeslotsJson = trainerInfoJson["timeslots"]
                    if timeslotsJson.count > 0 {
                        for j in 1...timeslotsJson.count {
                            let timeSlotJson = timeslotsJson[String(j)].rawString()
                            let timeSlot = TimeSlotInfo(json: timeSlotJson)
                            trainer.timeSlotInfoList.append(timeSlot)
                            trainer.timeSlotMap.updateValue(timeSlot.timeSlotID, forKey: (timeSlot.availLocalDate + timeSlot.slot))
                        }
                    }
                    
                    // Get places
                    let trainerPlacesJson = trainerInfoJson["trainerPlaces"]
                    if trainerPlacesJson.count > 0 {
                        for j in 1...trainerPlacesJson.count {
                            let trainerPlaceJson = trainerPlacesJson[String(j)].rawString()
                            let trainerPlace = TrainerPlaceInfo(json: trainerPlaceJson)
                            trainer.trainerPlaceInfoList.append(trainerPlace)
                        }
                    }
                    
                    // Get images
                    let imagesJson = trainerInfoJson["images"]
                    if imagesJson.count > 0 {
                        for j in 1...imagesJson.count {
                            let imageJson = imagesJson[String(j)].rawString()
                            let imageInfo = ImageInfo(json: imageJson)
                            trainer.imageInfoList.append(imageInfo)
                        }
                    }
                    
                    // Get reviews
                    let reviewsJson = trainerInfoJson["reviews"]
                    if reviewsJson.count > 0 {
                        for j in 1...reviewsJson.count {
                            let reviewJson = reviewsJson[String(j)].rawString()
                            let reviewInfo = ReviewInfo(json: reviewJson)
                            trainer.reviewInfoList.append(reviewInfo)
                        }
                    }
                    
                    trainers.append(trainer)
                }
                listener(trainers)
        }
    }
}